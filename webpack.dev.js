"use strict";

const path = require("path");

const paths = {
  SRC_DIR: path.resolve( __dirname , 'src' ) // ./src
};

const merge = require( 'webpack-merge' );
const common = require( './webpack.common.js' );

const webpack = require("webpack");
const wdefp = new webpack.DefinePlugin( {
  'process.env.NODE_ENV': JSON.stringify( 'development' )
} );
const hotModuleDev = new webpack.HotModuleReplacementPlugin({});

const config = {
  
  mode: "development" ,

  // devtool: 'inline-source-map' , 
  devtool: 'source-map' , 

  devServer: {
    // contentBase: paths.SRC_DIR 
    contentBase: paths.DIST_DIR 
    , inline: true
    , historyApiFallback: true 
    , hot: true 
    , host: '127.0.0.1'
    , port: 8080
    , open: true 
  } ,

  plugins: [ wdefp , hotModuleDev ]

};

module.exports = merge(common, config );
