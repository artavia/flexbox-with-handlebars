"use strict";

const merge = require( 'webpack-merge' );
const common = require( './webpack.common.js' );

const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const ujsp = new UglifyJSPlugin( {
  
  cache: true , 
  parallel: true , 
  
  sourceMap: true
} );

const webpack = require("webpack");
const wdefp = new webpack.DefinePlugin( {
  'process.env.NODE_ENV': JSON.stringify( 'production' )
} );

const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const opticss = new OptimizeCSSAssetsPlugin({});

const config = {
  
  mode: "production" ,

  // devtool: 'source-map' , 
  devtool: false , 

  plugins: [ ujsp , wdefp, opticss ]

};

module.exports = merge(common, config );
