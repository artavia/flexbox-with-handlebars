"use strict";

const path = require("path");

const paths = {
  ROOT_DIR: path.resolve( __dirname , '' ) // .
  , SRC_DIR: path.resolve( __dirname , 'src' ) // ./src
  , DIST_DIR: path.resolve( __dirname , 'dist' ) // ./dist
  , MODULES_DIR: path.resolve( __dirname , 'node_modules' ) // ./node_modules
};

const CleanWebpackPlugin = require("clean-webpack-plugin");
const cwp = new CleanWebpackPlugin( ['dist'] );

const HtmlWebpackPlugin = require("html-webpack-plugin");
const hwp = new HtmlWebpackPlugin( {
  filename: 'index.html'
  , template: paths.SRC_DIR + '/index.html'
} );

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractSass = new ExtractTextPlugin( {
  filename: 'css/styles.css'
} );

const config = {

  entry: {
    babblepolly: paths.SRC_DIR + '/app/polyfillobabble.js' , 
    main: paths.SRC_DIR + '/app/app.js'
    // , vendors: paths.SRC_DIR + '/app/vendorcode.js'
  } ,

  output: {
    filename: '[name].bundle.js'
    , path: paths.DIST_DIR
    , publicPath: ''
  } ,

  optimization: { 
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /([\\/]node_modules[\\/]|[\\/]src[\\/]app[\\/]|[\\/]src[\\/]app[\\/]components)/ 
          , chunks: 'all'
        }
      }
    } , 
    namedModules: true , 
    noEmitOnErrors: true , 
    concatenateModules: false // ModuleConcatenationPlugin // works if set to false and NOT true
  } , 

  module: {
    rules: [ 
      {
        test: /\.js$/
        , exclude: "/(node_modules|bower_components)/"
        , include: [
          paths.MODULES_DIR + "/handlebars/dist"
        ]
        , use: [
          {
            loader: 'babel-loader'
            , options: {
              presets: [ '@babel/preset-env' ] 
              // , plugins: ['transform-object-rest-spread'] 
            }
          }
        ]
      } 
      , {
        test: /\.scss$/ 
        , use: extractSass.extract( {
          fallback: 'style-loader'
          , use: [ 
            { 
              loader: "css-loader"
            } 
            , { 
              loader: "sass-loader"
            }  
          ]
        } )
      }
      , {
        test : /\.(png|jpg|gif)$/ 
        , use : [
          {
            loader : 'file-loader'
            , options: { 
              name: '[path][name].[ext]' 
              
              // , publicPath: '../images/' 
              // , outputPath: 'images/' // too complicated

              , publicPath: function(url) { 
                return url.replace( /src\/app\/images/, '../images'); 
              } 
              , outputPath: function(url) { 
                return url.replace( /src\/app\/images/, 'images/'); 
              }
            }
          }
        ]
      }
      , {
        test: /\.(ico)$/
        , use: [
          {
            loader: 'file-loader'
            , options: {
              name: '[name].[ext]'
              , outputPath: './'
            }
          }
        ]
      } 
      , {
        test: /\.handlebars$/
        , use: [
          {
            loader: "handlebars-loader"
            
            , options: { 
              helperDirs: [ paths.SRC_DIR + "/app/js/helpers" ] 
            }
          }
        ]
      }
      , {
        test: /\.json$/
        , loader: 'json-loader'
        , type: 'javascript/auto'
      } 
    ]
  } ,

  plugins: [ cwp , hwp , extractSass ]

};

module.exports = config; 