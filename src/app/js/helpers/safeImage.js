module.exports = function(pmName, pmImageUrl) {
  
  let Handlebars = require("handlebars/dist/handlebars");  
  
  pmName = this.name;
  pmImageUrl = this.photo;

  pmName = Handlebars.Utils.escapeExpression(pmName);
  pmImageUrl = Handlebars.Utils.escapeExpression(pmImageUrl);

  const result = `<img class=\"pirate-photo\" alt="Picture of one of ${pmName}\'s innermost memories" src="${pmImageUrl}" />`;

  return new Handlebars.SafeString( result );

}; 