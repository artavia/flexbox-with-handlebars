import JSONdata from "../data/pirates-4.json";
import HBT from "../templates/pirate.handlebars";

( function (){

  const piratesContainer = document.querySelector("#pirates-container");

  const loadFunc = () => {
    fetchThePiratesData( JSONdata );
  };

  const fetchThePiratesData = ( arr ) => {
    createHTML( arr );
  };
  
  const createHTML = (parsedResults) => {
    let html = HBT( { pirates: parsedResults } );
    piratesContainer.innerHTML = html;
    
  };

  window.onload = loadFunc;

})();
