# Description
Fun with basic CSS3 Flexbox and Handlebars.

## Please visit
You can see [the lesson at surge.sh](https://flexbox-with-handlebars.surge.sh "the lesson at surge") and decide if this is something for you to play around with at a later date.

## What it has
It makes use of the **Webpack 4** config files for each production and development. After some effort, I made sure that production mode did not break. It includes helper functions for use with the file-loader as they each apply to image public urls for use with css and the output folder names (which I think is self explanatory). 

## Baby steps to my final (choreographed) objective
A lot of thought went into this so that there could be some takeaway for the audience. Here are some of the steps that I took:
  - I watched [not one](https://www.youtube.com/watch?v=DG4obitDvUA "link to JavaScript Template Literals: JSON to HTML") nor [two](https://www.youtube.com/watch?v=rJesac0_Ftw "link to JSON and AJAX Tutorial: With Real Examples") but [three different videos](https://www.youtube.com/watch?v=wSNa5b1mS5Y "link to Handlebars.js Tutorial") on the subject of templating to get the creative juices going;
  - I read an article on [CSS for dinosaurs](https://medium.com/actualize-network/modern-css-explained-for-dinosaurs-5226febe3525 "link to Modern CSS Explained For Dinosaurs") (like myself) which again pushed me to revisit each CSS3 flexbox and CSS3 grid once again;
  - Around the end of July 2018, I had put together an unpublished work that was based on a pair of videos [(link to part 1)](https://www.youtube.com/watch?v=I8NYMnpyyY0 "Link to Learn JQuery in 30 days") by Jeffrey Way from 2012. The subject was JQuery with PHP [(link to part 2)](https://www.youtube.com/watch?v=IbZa0Rxm8DQ "Link to Learn JQuery in 30 days"). I took the additional step of externalizing the handlebars templates and was looking for a reason to publish my findings. I **subsequently** built [another project](https://gitlab.com/artavia/ajax-for-handlebars-and-json "link to Fun with Ajax For Handlebars and JSON data") that **again permitted me to apply my same technique** but with a json response instead of a mysql response. 

In sum, **this specific work** is the brainchild that resulted from completing all of the aforementioned steps!

## Helpful links that got me over the hump 
These are going to come at you in no particular order:

 - When you download Handlebars with npm, you are presented with a lot of choices. [This link helped me](https://github.com/wycats/handlebars.js/issues/1174#issuecomment-229918935 "link to github") to narrow the list down significantly;
 - The Webpack api guide at [js dot org](https://webpack.js.org/guides/production/ "link to production best practices") is stoked with little nuggets of info and ideas. The lesson is still the same but some of the finer details may have changed;
 - This was the **most helpful link by far** because it was instrumental in helping me to [cure the production mode bug](https://dev.to/flexdinesh/upgrade-to-webpack-4---5bc5 "link to Upgrade to Webpack4"). The only difference is that I used **concatenateModules: false** instead of true. This also cleared up the airs in terms of what is being phased out in terms of plugins and modules;
 - I liked [this link](https://github.com/webpack-contrib/file-loader/issues/160#issuecomment-306215576 "link to github") because of its novelty. It gave me the idea to apply a helper function for each the **outputPath** and **publicPath** fields;
 - Although the difference could be nominal, the idea for [importing the images](https://stackoverflow.com/questions/47614274/use-index-js-to-import-multiple-image-assets-in-react "link to stackoverflow") came from StackOverflow;
 - Just because I did not [fork a copy](https://github.com/LearnWebCode/handlebars-webpack "link to Learn Web Code handlebars example") does not mean the original work does not warrant a nod. The example was too simplistic for my needs but it did perpetuate this recent Handlebars kick that I have been on for a couple of months. I can easily end up doing the same thing for each **Knockout** and [Mustache](https://gitlab.com/artavia/ajax-for-mustache-and-json "link to Fun with Ajax For Mustache and JSON data") if I really want to someday!

## Artistic Disclaimer
All file names are final. I realize that there can be some dissonance between what is described and what is presented but those details are so small that anybody who cannot appreciate the lesson or the thought and care that went into it is just being petty. In my own personal codebase, the .json and the .handlebars file names make sense.

## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!